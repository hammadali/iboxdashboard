@extends('adminlte::page')

@section('title', 'ibox Dashboard')

@section('content_header')
    <h1>Rooms</h1>
@stop

@section('content')
    
<div class="box box-primary">
  <div class="box-header with-border">
   <h3 class="box-title">Edit Room</h3>
 </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

 <!-- /.box-header -->
 <!-- form start -->
{!! Form::open(['url' => 'room/'.$roomsData->room_id, 'name' => 'rooms']) !!}
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Room No</label>
          {!! Form::input('text', 'room_no', $roomsData->room_no, ['class' => 'form-control']) !!}
        </div>
      </div>


      <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
          <label>Room Floor</label>
         <select name="room_floor" class="form-control"> 
           <option value="Ground Floor">Ground Floor</option>
           <option value="1st Floor" <?php if($roomsData->room_floor == '1st Floor'){ echo 'selected';} ?> >1st Floor</option>
           <option value="2nd Floor" <?php if($roomsData->room_floor == '2nd Floor'){ echo 'selected';} ?>>2nd Floor</option>
           <option value="3rd Floor" <?php if($roomsData->room_floor == '3rd Floor'){ echo 'selected';} ?>>3rd Floor</option>
           <option value="4th Floor" <?php if($roomsData->room_floor == '4th Floor'){ echo 'selected';} ?>>4th Floor</option>
           <option value="5th Floor" <?php if($roomsData->room_floor == '5th Floor'){ echo 'selected';} ?>>5th Floor</option>
         </select>
        </div>
        <!-- /.form-group -->
      </div>


      <div class="col-md-6">
        <!-- /.form-group -->

        <div class="form-group">
          <label>Room Type</label>
           <select name="room_type" class="form-control"> 
           <option value="Ward" <?php if($roomsData->room_type == 'Ward'){ echo 'selected';} ?>>Ward</option>
           <option value="Private Room" <?php if($roomsData->room_type == 'Private Room'){ echo 'selected';} ?>>Private Room </option>
           <option value="Executive Room" <?php if($roomsData->room_type == 'Executive Room'){ echo 'selected';} ?>>Executive Room </option>
         </select>
        
        </div>

        <!-- /.form-group -->
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label>No of Beds</label>
          <select name="room_beds" class="form-control"> 
           <option value="1" <?php if($roomsData->room_beds == 1){ echo 'selected';} ?>> 1 </option>
           <option value="2" <?php if($roomsData->room_beds == 2){ echo 'selected';} ?>> 2</option>
           <option value="5" <?php if($roomsData->room_beds == 5){ echo 'selected';} ?>> 5 </option>
           <option value="10" <?php if($roomsData->room_beds == 10){ echo 'selected';} ?>> 10 </option>
           <option value="15" <?php if($roomsData->room_beds == 15){ echo 'selected';} ?>> 15 </option>
           <option value="20" <?php if($roomsData->room_beds == 20){ echo 'selected';} ?>> 20 </option>
         </select>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label>Room Price</label>
          {!! Form::input('text', 'room_price', $roomsData->room_price, ['class' => 'form-control']) !!}
        </div>
      </div>

     <div class="col-md-6">
        <div class="form-group">
          <label>Clinic</label>
 
          {{ Form::select('clinic_id', $clinicInfo, $roomsData->clinic_id ,array('class' => 'form-control')) }}
          
        </div>
     </div>
      

      <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
          <label>Room Status</label>

          <select name="room_status" class="form-control"> 
           <option value="0" <?php if($roomsData->room_status == 0){ echo 'selected';} ?>>Free</option>
           <option value="1" <?php if($roomsData->room_status == 1){ echo 'selected';} ?>>Captured</option>
          </select>

       </div>
       <!-- /.form-group -->
     </div>

     <div class="col-md-6">
        <div class="form-group">
          <label>Room Description</label>
         
          {!! Form::textarea('room_description',$roomsData->room_description,['class'=>'form-control', 'rows' => 5, 'cols' => 30]) !!}
        </div>
      </div>

   </div>
   <!-- /.row -->
 </div>
 <!-- /.box-body -->

 <div class="box-footer">
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="list" class="btn btn-default">Cancel</a>
</div>
{!! Form::close() !!}
</div>


@stop