@extends('adminlte::page')

@section('title', 'ibox Dashboard')

@section('content_header')
    <h1>Rooms</h1>
@stop

@section('content')
    
<div class="box box-primary">
  <div class="box-header with-border">
   <h3 class="box-title">Add New Room</h3>
 </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

 <!-- /.box-header -->
 <!-- form start -->
{!! Form::open(['url' => 'room', 'name' => 'rooms']) !!}
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Room No</label>
          {!! Form::input('text', 'room_no', null, ['class' => 'form-control']) !!}
        </div>
      </div>


      <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
          <label>Room Floor</label>
         <select name="room_floor" class="form-control"> 
           <option value="Ground Floor">Ground Floor</option>
           <option value="1st Floor">1st Floor</option>
           <option value="2nd Floor">2nd Floor</option>
           <option value="3rd Floor">3rd Floor</option>
           <option value="4th Floor">4th Floor</option>
           <option value="5th Floor">5th Floor</option>
         </select>
        </div>
        <!-- /.form-group -->
      </div>


      <div class="col-md-6">
        <!-- /.form-group -->

        <div class="form-group">
          <label>Room Type</label>
           <select name="room_type" class="form-control"> 
           <option value="Ward">Ward</option>
           <option value="Private Room">Private Room </option>
           <option value="Executive Room">Executive Room </option>
         </select>
        
        </div>

        <!-- /.form-group -->
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label>No of Beds</label>
          <select name="room_beds" class="form-control"> 
           <option value="1"> 1 </option>
           <option value="2"> 2</option>
           <option value="5"> 5 </option>
           <option value="10"> 10 </option>
           <option value="15"> 15 </option>
           <option value="20"> 20 </option>
         </select>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label>Room Price</label>
          {!! Form::input('text', 'room_price', null, ['class' => 'form-control']) !!}
        </div>
      </div>

     <div class="col-md-6">
        <div class="form-group">
          <label>Clinic</label>
 
          {{ Form::select('clinic_id', $clinicInfo, null ,array('class' => 'form-control')) }}
          
        </div>
     </div>
      

      <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
          <label>Room Status</label>

          <select name="room_status" class="form-control"> 
           <option value="0">Free</option>
           <option value="1">Captured</option>
          </select>

       </div>
       <!-- /.form-group -->
     </div>

     <div class="col-md-6">
        <div class="form-group">
          <label>Room Description</label>
         
          {!! Form::textarea('room_description',null,['class'=>'form-control', 'rows' => 5, 'cols' => 30]) !!}
        </div>
      </div>

   </div>
   <!-- /.row -->
 </div>
 <!-- /.box-body -->

 <div class="box-footer">
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="list" class="btn btn-default">Cancel</a>
</div>
{!! Form::close() !!}
</div>


@stop