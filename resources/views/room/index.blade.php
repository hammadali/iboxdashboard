@extends('adminlte::page')

@section('title', 'ibox Dashboard')

@section('content_header')
    <h1>Rooms</h1>
@stop


@section('content')
    


<div class="box">
  <div class="box-header">
    <h3 class="box-title">Listing</h3>

    <div class="box-tools">
      <a href="create" class="btn btn-block btn-primary"> Add New Room </a>
     
    </div>
  </div>


   @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
   @endif
  <!-- /.box-header -->
  <div class="box-body table-responsive no-padding">
    <table class="table table-hover"  id="example">
      <tbody><tr>

        <th>Room No.</th>
        <th>Floor</th>
        <th>Room Type</th>
        <th>No. of Beds</th>
        <th>Room Description</th>
        <th>Clinic Name</th>
        <th>Room Status</th>
        <th>Actions</th>
      </tr>

      @if(isset($roomsData))

      @foreach ($roomsData as $row)

      <tr>

        <td>{{ $row->room_no }}</td>
        <td>{{ $row->room_floor }}</td>
        <td>{{ $row->room_type }}</td>
        <td>{{ $row->room_beds }}</td>
       
        <td>{{ $row->room_description }}</td>
        <td>{{ $row->clinic['clinic_name'] }} </td>
        <td>
          @if($row->room_status == 1)
          <span class="label label-danger">Captured</span>
          @else
          <span class="label label-success">Free</span>
          @endif
        </td>

        <td>
          <a href="edit/{{ $row->room_id }}"> <span class="glyphicon glyphicon-edit"></span> </a>
          <a href="javascript:;" onclick="del_room(<?php echo $row->room_id ?>)"><span class="glyphicon glyphicon-trash"></span></a>
        </td>
      </tr>
      @endforeach

      @else
      <tr> <td colspan="5">No Recorrd Found</td> </tr>

      @endif

    </tbody></table>
  </div>
  <!-- /.box-body -->
</div>

<script type="text/javascript">
  
  function del_room(room_id){

   if (confirm('Are you sure to delete?')) {
    
     window.location.href = "destroy/"+room_id;

    } 


  }

</script>


@stop




