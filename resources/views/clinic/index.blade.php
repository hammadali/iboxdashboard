@extends('adminlte::page')

@section('title', 'ibox Dashboard')

@section('content_header')
    <h1>Clinics</h1>
@stop


@section('content')
    


<div class="box">
  <div class="box-header">
    <h3 class="box-title">Listing</h3>

    <div class="box-tools">
      <a href="create" class="btn btn-block btn-primary"> Add New Clinic </a>
     
    </div>
  </div>


   @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
   @endif
  <!-- /.box-header -->
  <div class="box-body table-responsive no-padding">
    <table class="table table-hover"  id="example">
      <tbody><tr>

        <th>Clinic Name</th>
        <th>Clinic Address</th>
        <th>Clinic Phone</th>
        <th>Clinic Status</th>
        <th>Actions</th>
      </tr>

      @if(isset($clinics))

      @foreach ($clinics as $row)

      <tr>

        <td>{{ $row->clinic_name }}</td>
        <td>{{ $row->clinic_address }}</td>
        <td>{{ $row->clinic_phone }}</td>
        <td>
          @if($row->clinic_status == 1)
          <span class="label label-success">Active</span></td>
          @else
          <span class="label label-danger">In Active</span></td>
          @endif
        <td>
          <a href="edit/{{ $row->clinic_id }}"> <span class="glyphicon glyphicon-edit"></span> </a>
          <a href="javascript:;" onclick="del_clinic(<?php echo $row->clinic_id ?>)"><span class="glyphicon glyphicon-trash"></span></a>
        </td>


      </tr>
      @endforeach

      @else
      <tr> <td colspan="5">No Recorrd Found</td> </tr>

      @endif

    </tbody></table>
  </div>
  <!-- /.box-body -->
</div>

<script type="text/javascript">
  
  function del_clinic(id){

   if (confirm('Are you sure to delete?')) {
    
     window.location.href = "destroy/"+id;

    } 


  }

</script>


@stop




