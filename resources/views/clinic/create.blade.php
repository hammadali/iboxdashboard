@extends('adminlte::page')

@section('title', 'ibox Dashboard')

@section('content_header')
    <h1>Clinics</h1>
@stop

@section('content')
    
<div class="box box-primary">
  <div class="box-header with-border">
   <h3 class="box-title">Add New Clinic</h3>
 </div>

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

 <!-- /.box-header -->
 <!-- form start -->
{!! Form::open(['url' => 'clinic', 'name' => 'clinics']) !!}
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Clinic Name</label>
          {!! Form::input('text', 'clinic_name', null, ['class' => 'form-control']) !!}
        </div>
      </div>


      <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
          <label>Clinic Phone</label>
          {!! Form::input('text', 'clinic_phone', null, ['class' => 'form-control']) !!}
        </div>
        <!-- /.form-group -->
      </div>


      <div class="col-md-6">
        <!-- /.form-group -->

        <div class="form-group">
          <label>Clinic Address</label>
          {!! Form::textarea('clinic_address',null,['class'=>'form-control', 'rows' => 5, 'cols' => 30]) !!}
        </div>

        <!-- /.form-group -->
      </div>



      <div class="col-md-6">
        <!-- /.form-group -->
        <div class="form-group">
          <label>Clinic status</label>

          <select name="clinic_status" class="form-control"> 
           <option value="1">Active</option>
           <option value="0">In Active</option>
         </select>

       </div>
       <!-- /.form-group -->
     </div>

   </div>
   <!-- /.row -->
 </div>
 <!-- /.box-body -->

 <div class="box-footer">
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="list" class="btn btn-default">Cancel</a>
</div>
{!! Form::close() !!}
</div>


@stop