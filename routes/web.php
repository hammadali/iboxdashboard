<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'DashboardController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Clinic Route Starts

Route::get('clinic/list','ClinicController@index');
Route::get('clinic/create','ClinicController@create');
Route::post('clinic','ClinicController@store');
Route::get('clinic/edit/{clinic_id}', 'ClinicController@edit');
Route::post('clinic/{clinic_id}','ClinicController@update');
Route::get('clinic/destroy/{clinic_id}','ClinicController@destroy');

// Clinic Routes Ends


// Room Route Starts

Route::get('room/list','RoomController@index');
Route::get('room/create','RoomController@create');
Route::post('room','RoomController@store');
Route::get('room/edit/{room_id}', 'RoomController@edit');
Route::post('room/{room_id}','RoomController@update');
Route::get('room/destroy/{room_id}','RoomController@destroy');

// Room Routes Ends