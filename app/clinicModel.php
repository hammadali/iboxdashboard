<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clinicModel extends Model
{
    

	protected $table = "clinics";
 	protected $primaryKey = 'clinic_id';
   
    public $timestamps = true;

 	protected $fillable = [
        'clinic_id', 
        'clinic_name',
        'clinic_address',
        'clinic_phone',
        'clinic_status',
        'user_id'
    ];


	/**
	 * Get the rooms for the clinic.
	 */

    public function rooms()
    {
        return $this->hasMany('App\roomsModel','clinic_id');
    }


}
