<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\clinicModel;

class ClinicController extends Controller
{

  /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

      $clinics  = clinicModel::get();
       return view('clinic/index')->with(compact('clinics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {  
       return view('clinic/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
         $request->validate([
         	'clinic_name' => 'required|unique:clinics|max:100',
         	'clinic_phone' => 'required|numeric',
         	'clinic_address' => 'required|max:100',
         ]);
  		

	  		$data = $request->all();
	  		
	  		$user = auth()->user();
	        $data['user_id'] = $user->id;
		    
         $response = clinicModel::create($data);
		 
		 return redirect('clinic/list')->with('message', 'Clinic Added Successfully');
    }

 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	     $clinic = clinicModel::where('clinic_id',  '=', $id)->first();
		 return view('clinic/edit', compact('clinic')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id , Request $request)
    {

        $request->validate([
         	'clinic_name' => 'required|unique:clinics,clinic_name,'.$id.',clinic_id|max:100',
         	'clinic_phone' => 'required|numeric',
         	'clinic_address' => 'required|max:100',
         ]);
  		
	  	 $data = $request->all();
	  	 unset( $data['_token']);
	  	
	  		/*$user = auth()->user();
	        $data['user_id'] = $user->id; */
		    
         $response = clinicModel::where('clinic_id' ,'=', $id)->update($data);
		 
		 return redirect('clinic/list')->with('message', 'Clinic Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       
       $clinicModel = clinicModel::where('clinic_id' ,'=', $id);

       if($clinicModel){

          $clinicModel->delete();
          return redirect('clinic/list')->with('message', 'Clinic Deleted Successfully');
         }else{

          return redirect('clinic/list')->with('error', 'Clinic Not Deleted');
         }
    }

}
