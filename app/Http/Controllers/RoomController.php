<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\clinicModel;
use App\roomsModel;

class RoomController extends Controller
{
   

/**
 * Display a listing of the resource.
 *
 * @return Response
 */
public function index()
{

  $roomsData  = roomsModel::with('clinic')->get();

   return view('room/index')->with(compact('roomsData'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return Response
 */
public function create()
{  
   $clinicInfo = clinicModel::where('clinic_status', 1)->pluck('clinic_name', 'clinic_id')->all();
   return view('room/create')->with(compact('clinicInfo'));
}

/**
 * Store a newly created resource in storage.
 *
 * @return Response
 */
public function store(Request $request)
{

     $request->validate([
     	
     	'room_no' => 'required|unique:rooms,room_no|max:10',
     	'room_floor' => 'max:50',
     	'room_type' => 'max:30',
     	'room_price' => 'required',
     ]);
		
	$data = $request->all();
	
	$user = auth()->user();
    $data['user_id'] = $user->id;
	 
    $response = roomsModel::create($data);
	 
	return redirect('room/list')->with('message', 'Room Added Successfully');
}


/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return Response
 */
public function edit($room_id)
{
     $roomsData = roomsModel::where('room_id',  '=', $room_id)->first();
     $clinicInfo = clinicModel::where('clinic_status', 1)->pluck('clinic_name', 'clinic_id')->all();
	 return view('room/edit', compact('roomsData', 'clinicInfo')); 
}

/**
 * Update the specified resource in storage.
 *
 * @param  int  $room_id
 * @return Response
 */
public function update($room_id , Request $request)
{
      
      $request->validate([
     	
     	'room_no' => 'required|unique:rooms,room_no,'.$room_id.',room_id|max:10',
     	'room_floor' => 'max:50',
     	'room_type' => 'max:30',
     	'room_price' => 'required',
     ]);
		
  	 $data = $request->all();
  	 unset( $data['_token']);
  	
		/*$user = auth()->user();
	    $data['user_id'] = $user->id; */
	    
     $response = roomsModel::where('room_id' ,'=', $room_id)->update($data);
	 
	 return redirect('room/list')->with('message', 'Room Updated Successfully');
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return Response
 */
public function destroy($room_id)
{
   
   $roomsModel = roomsModel::where('room_id' ,'=', $room_id);

   if($roomsModel){

      $roomsModel->delete();
      return redirect('room/list')->with('message', 'Room Deleted Successfully');
     }else{

      return redirect('room/list')->with('error', 'Room Not Deleted');
     }
}


}
