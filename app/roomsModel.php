<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roomsModel extends Model
{
    
	protected $table = "rooms";
	protected $primaryKey = 'room_id';

	public $timestamps = true;

	protected $fillable = [
    'room_id', 
    'room_no',
    'room_floor',
    'room_type',
    'room_beds',
    'room_status',
    'room_price',
    'room_description',
    'clinic_id',
    'user_id'
    ];


    /**
     * Get the post that owns the comment.
     */
    public function clinic()
    {
        return $this->belongsTo('App\clinicModel','clinic_id');
    }


}
